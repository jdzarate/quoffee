from django.shortcuts import redirect
from django.conf import settings
from social.pipeline.partial import partial
from quoffee.quotes.models import Notebook
from quoffee.users.models import Profile, RegistrationProfile
from quoffee.utils.methods import generate_uniquekey

@partial
def require_email(strategy, details, user=None, is_new=False, *args, **kwargs):
	if user and user.email:
		return
	elif is_new and not details.get('email'):
		if strategy.session_get('saved_email'):
			details['email'] = strategy.session_pop('saved_email')
		else:
			return redirect('require_email')

@partial
def update_details(strategy, details, user, is_new=False, *args, **kwargs):
	if is_new and user is not None:
		display = None
		try:
			if user.social_auth.get(provider="twitter"):
				display = user.social_auth.get(provider='twitter').extra_data['profile_image_url']
				display = display.replace("_normal", "_bigger")
			elif user.social_auth.get(provider="facebook"):
				display = "http://graph.facebook.com/%s/picture?" % user.social_auth.get(provider='facebook').uid
		except:
			display = None
		notebook = Notebook(user=user, name="My awesome first notebook", uniquekey=generate_uniquekey(getattr(settings, 'NOTEBOOK_KEY_LENGTH')))
		notebook.save()
		profile = Profile(user=user, display=display, default_notebook=notebook, activated=True)
		profile.save()

	if not is_new and user is not None:
		try:
			display = user.social_auth.get(provider='twitter').extra_data['profile_image_url']
			display = display.replace("_normal", "_bigger")
			profile = Profile.objects.get(user=user)
			profile.display = display
			profile.save()
		except:
			pass

		#registration_profile = RegistrationProfile(user=user, activation_key=generate_uniquekey(30), activation_email=user.email, activated=False)
		#registration_profile.save()
		# backend = strategy.session_get('partial_pipeline')['backend']
  #       return redirect('social:complete', backend=backend)