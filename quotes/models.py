from rest_framework import serializers
from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from quoffee.context.models import Author, Category
from quoffee.notifications.models import Notification

# Create your models here.
class Notebook(models.Model):
	user = models.ForeignKey(User)
	name = models.CharField(max_length=100)
	uniquekey = models.CharField(max_length=4)

class Quote(models.Model):
	user = models.ForeignKey(User)
	uniquekey = models.CharField(max_length=32)
	text = models.TextField()
	author = models.ForeignKey(Author, null=True, blank=True)
	category = models.ForeignKey(Category, null=True, blank=True)
	custom_category = models.CharField(max_length=100, null=True, blank=True) # for custom categories only.
	requoted = models.BooleanField(default=False)
	requote_count = models.IntegerField(null=True, blank=True,default=0)
	added_date = models.DateTimeField(auto_now_add=True)
	deleted = models.BooleanField(default=False)
	notebook = models.ForeignKey(Notebook)
	meta = models.TextField(null=True, blank=True)
	url = models.CharField(max_length=150, null=True, blank=True)
	

class ReQuote(models.Model):
	quote = models.ForeignKey(Quote)
	user = models.ForeignKey(User) #user that requoted the quote.
	requote_date = models.DateTimeField(auto_now_add=True)
	deleted = models.BooleanField(default=False)
	requote = models.ForeignKey(Quote, related_name='quote_requote', null=True, blank=True)

'''-----------
	Signals
------------'''
@receiver(post_save, sender=ReQuote)
def requote_notification(sender, instance, created,**kwargs):
	if created:
		notifications = Notification(user=instance.quote.user, notif_type=1, notif_item=instance.quote.id, sender=instance.user)
		notifications.save()