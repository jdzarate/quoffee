from django.conf.urls import patterns, url

urlpatterns = patterns('quoffee.quotes.views',
	url(r'^api/quotes/insert/', 'insert_quote', name='insert_quote'),
	url(r'^api/quotes/delete/', 'delete_quote', name='delete_quote'),
	url(r'^api/quotes/requote/', 'requote', name='requote'),
	url(r'^api/quotes/change-notebook/', 'change_notebook', name='change_notebook'),
	url(r'^api/quotes/get/index', 'get_index_quotes', name='get_index_quotes'),
	url(r'^q/(?P<uniquekey>\w+)/', 'get_quote_page', name='get_quote_page'),
	url(r'^quote/get/', 'get_quote_card', name='get_quote_card'),
	url(r'^quotes/get/', 'get_quote_data', name='get_quote_data'),
	url(r'^notebooks/get/', 'get_notebook_list', name='get_notebook_list'),
	url(r'^api/notebooks/(?P<action>\w+)/', 'notebook_crud', name='notebook_crud'),

)