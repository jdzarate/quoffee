 # -*- coding: utf-8 -*-
import json, hashlib, urllib
from django.conf import settings
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django.http import HttpResponse, Http404
from django.shortcuts import render, render_to_response, redirect
from django.template import RequestContext
from social.backends.google import GooglePlusAuth
from quoffee.quotes.models import Notebook, Quote, ReQuote
from quoffee.users.models import Profile
from quoffee.context.models import Author, Category
from quoffee.tags.models import TagMap, Tag
from quoffee.utils.serializers import ReQuoteSerializer
from quoffee.utils.methods import generate_uniquekey, get_quote_display_data, get_quote_page_data, get_notebook_display_data, slugify_unique as slugify, get_is_user_context_data

# Create your views here.
def index(request):
    # scope = ' '.join(GooglePlusAuth.DEFAULT_SCOPE)
    # context = {
    #     'user': request.user,
    #     'plus_id': getattr(settings, 'SOCIAL_AUTH_GOOGLE_PLUS_KEY', None),
    #     'plus_scope': scope
    # }
    is_user = request.user.is_authenticated()
    if request.method=='GET':
        if request.GET.get('bm')=='on':
            if is_user:
                quote = request.GET.get('quote') if request.GET.get('quote') else ''
                url = request.GET.get('url') if request.GET.get('url') else ''
                profile = Profile.objects.get(user=request.user)
                notebooks = get_notebook_display_data(Notebook.objects.filter(user=request.user), profile.default_notebook)
                context_data = {
                        'quote': urllib.unquote(quote),
                        'url': url,
                        'notebooks': notebooks,
                        'profile': profile,
                        'categories': Category.objects.all().order_by('name'),
                        'is_bookmarklet': True,
                    }
                return render_to_response('bookmarklet/bookmarklet_dialog.html', context_data, RequestContext(request))
            else:
                return render_to_response('bookmarklet/bookmarklet_login.html', None, RequestContext(request))
        else:
            if is_user:
            	context_data = get_is_user_context_data(request.user)
                context_data['categories'] = Category.objects.all().order_by('name')
            else:
            	context_data = {'is_user': False}
            return render_to_response('index.html', context_data, RequestContext(request))
    else:
        raise Http404


def get_index_quotes(request):
    quotes = Quote.objects.filter(deleted=False, requoted=False).order_by('-added_date')[:70]
    quote_array = list()
    next_page = False
    p = Paginator(quotes, 10)
    try:
        this_page = int(request.GET.get('page')) if request.GET.get('page') else 1
        page_quotes = p.page(this_page)
        next_page = this_page+1 if page_quotes.has_next() else False
        for item in page_quotes:
            quote_array.append(get_quote_display_data(item))
    except (ValueError, EmptyPage, InvalidPage):
        pass
    context_data = {'quotes': quote_array, 'next_page': next_page, 'path': request.path}
    return render_to_response('quote-display-data.html', context_data, RequestContext(request))


def bookmarklet_page(request):
    if request.method == 'GET':
        is_user = request.user.is_authenticated()
        context_data = get_is_user_context_data(request.user) if is_user else {'is_user': False}
        return render_to_response('bookmarklet.html', context_data, RequestContext(request))
    else:
        raise Http404

def get_quote_page(request, uniquekey):
    public = True
    context_data = None
    is_user = request.user.is_authenticated()
    if request.method=='GET':
        if is_user:
            profile = Profile.objects.get(user=request.user)
            notebooks = get_notebook_display_data(Notebook.objects.filter(user=request.user), profile.default_notebook)
            user = dict(id=request.user.id, name=request.user.first_name+' '+request.user.last_name, display=profile.display, username=request.user.username)        
            tagmap = TagMap.objects.values_list('tag_id', flat=True).filter(quote__user=request.user)
            tags = Tag.objects.filter(id__in=tagmap)
            context_data = {'user': user, 'is_user': is_user, 'notebooks': notebooks, 'profile': profile,'tags': tags}        
        try:
            quote = Quote.objects.get(uniquekey=uniquekey, deleted=False)
            if context_data:
                context_data['data'] = get_quote_page_data(request.user, quote)
            else:
                context_data = {'data': get_quote_page_data(request.user, quote),'is_user': is_user}
        except Quote.DoesNotExist:
            quote_data = None
    else:
        context_data = None
    return render_to_response('quote-page.html', context_data, RequestContext(request))


def insert_quote(request):
    errors = list()
    message = None
    uniquekey = None
    if request.method == 'POST' and request.user.is_authenticated():
        profile = Profile.objects.get(user=request.user)
        form = request.POST
        #notebook stuff.
        is_new_notebook = False
        new_notebook_data = None
        selected_notebook = form.get('newnotebook')
        if selected_notebook and len(selected_notebook)>0:
            notebook_max_length = getattr(settings,'NOTEBOOK_NAME_LENGTH')
            selected_notebook = (selected_notebook[:notebook_max_length]) if len(selected_notebook) > notebook_max_length else selected_notebook
            if profile.notebook_count < profile.notebook_limit:
                notebook = Notebook(name=selected_notebook, user=request.user, uniquekey=generate_uniquekey(getattr(settings, 'NOTEBOOK_KEY_LENGTH')))
                notebook.save()
                profile.notebook_count = profile.notebook_count + 1
                profile.save()
                is_new_notebook = True
                new_notebook_data = {'id': notebook.id, 'name': notebook.name}
            else:
                errors.append("That's a bummer but you exceeded your notebook limit.")
        else:
            try:
                notebook = Notebook.objects.get(uniquekey=form.get("notebook"), user=request.user)
            except Notebook.DoesNotExist:
                errors.append("Hey. The selected notebook is not yours!")
        #category stuff.
        selected_category = form.get('category')
        if selected_category:
            try:
                if isinstance(int(selected_category), (int, long)):
                    try:
                        category = Category.objects.get(pk=selected_category)
                        custom_category = False
                    except Category.DoesNotExist:
                        errors.append('Sorry, but the last time we checked numbers are not categories.')
            except ValueError: # is not a integer.
                custom_category = True
        #author stuff.
        selected_author = form.get('author')
        author = None
        if selected_author:
            try:
                if isinstance(int(selected_author), (int, long)):
                    try:
                        author = Author.objects.get(pk=selected_author)
                    except Author.DoesNotExist:
                        errors.append('Sorry, but the last time we checked numbers are not authors.')
            except ValueError:
                    try:
                        author = Author.objects.get(name__iexact=selected_author)
                    except Author.DoesNotExist:
                        author = Author(name=selected_author, user=request.user)
                        author.save()
        #quote stuff
        if len(errors) == 0:
            quote = Quote(user=request.user,uniquekey=generate_uniquekey(getattr(settings, 'QUOTE_KEY_LENGTH')), author=author, notebook=notebook, text=form.get('text'))
            if selected_category:
                if custom_category:
                    quote.custom_category = selected_category
                else:
                    quote.category = category
            if form.get('url'):
                quote.url = form.get('url')
            quote.save()
            uniquekey = quote.uniquekey
            message = "Behold! You have a brand new quote!"
            if author:
                author.quote_count = author.quote_count + 1
                author.save()
                #save author quote log. here <----------------------------------------------------------------------------
            #tags stuff
            selected_tags = form.get('tags')
            if selected_tags:
                tags_array = selected_tags.split(',')
                for item in tags_array:
                    #search for an existing tag, if it does not exists, create a new one.
                    item = item.lower()
                    try:
                        tag = Tag.objects.get(name=item)
                    except Tag.DoesNotExist:
                        tag = Tag(name=item, user=request.user, slug=slugify(item,Tag))
                        tag.save()
                    tagmap = TagMap(tag=tag, quote=quote)
                    tagmap.save()
    else:
        errors.append("We are sorry but only authenticated users can create new quotes.")
    response = {'valid': (len(errors)==0), 'errors':errors, 'is_new_notebook': is_new_notebook, 'new_notebook_data': new_notebook_data, 'quotekey': uniquekey, 'message': message}
    json_response = json.dumps(response)
    return HttpResponse(json_response, content_type='application/javascript')


def delete_quote(request):
    errors = list()
    message = None
    if request.method=="POST" and request.user.is_authenticated():
        try:
            quote = Quote.objects.get(uniquekey=request.POST.get('key'), user=request.user)
            quote.deleted = True if not request.POST.get('undo') else False
            quote.save()
            if quote.requoted:
                requote = ReQuote.objects.get(requote=quote, user=request.user)
                requote.deleted = True if not request.POST.get('undo') else False
                requote.save()
            if request.POST.get('undo'):
                message = "That was close!"
            else:
                message = "... and that quote is out of here! What? NO, NO, NO!"
        except Quote.DoesNotExist:
            errors.append("What quote?")
    response = {'valid': (len(errors)==0), 'errors': '• '.join(errors), 'key': request.POST.get('key'), 'message': message}
    json_response = json.dumps(response)
    return HttpResponse(json_response, content_type='application/javascript')       

def requote(request):
    # we need quoteid and user notebook (or default notebook).
    errors = list()
    message = None
    activation_error = False
    if request.method=="POST" and request.user.is_authenticated():
        try:
            #requote to default notebook.
            profile = Profile.objects.get(user=request.user)
            if profile.activated:
                notebook = profile.default_notebook
                quote = Quote.objects.get(uniquekey=request.POST.get('key'))
                quote.requote_count = quote.requote_count + 1
                uniquekey = generate_uniquekey(getattr(settings, 'QUOTE_KEY_LENGTH'))
                if not ReQuote.objects.filter(quote=quote,user=request.user, deleted=False).exists():
                    requote = Quote(user=request.user, text=quote.text, requoted=True, author=quote.author, uniquekey=uniquekey, 
                        category=quote.category, custom_category=quote.custom_category,notebook=notebook)
                    requote.save()
                    requote_log = ReQuote(quote=quote, user=request.user, requote=requote)
                    requote_log.save()
                    message = "Success! You requoted the hell out of it!"
                else:
                    errors.append("Great! but you have already requoted this one.")
            else:
                errors.append("You must activate your account first.")
                activation_error = True
        except Quote.DoesNotExist:
            errors.append("We didn't find the quote you want to requote.")
    response = {'valid': (len(errors)==0), 'errors': '• '.join(errors), 'message': message, 'activation_error': activation_error}    
    json_response = json.dumps(response)
    return HttpResponse(json_response, content_type='application/javascript')


def get_quote_card(request):
    quote_display_data = None
    if request.method=='GET' and request.user.is_authenticated():
        key = request.GET.get('uniquekey')
        try:
            quote = Quote.objects.get(uniquekey=request.GET.get('uniquekey'))
            quote_display_data = [get_quote_display_data(quote)]
        except Quote.DoesNotExist:
            pass
    return render_to_response('quote-display-data.html', {'quotes': quote_display_data}, RequestContext(request))

def get_quote_data(request):
    data = None
    is_user = request.user.is_authenticated()
    if request.method=='GET':
        try:
            quote = Quote.objects.get(uniquekey=request.GET.get('key'))
            data = get_quote_page_data(request.user, quote)
        except Quote.DoesNotExist:
            pass
    return render_to_response('quote-panel-data.html', {'data': data, 'is_user': is_user}, RequestContext(request))

'''--------------------
    Notebook Views
--------------------'''
def get_notebook_list(request):
    if request.user.is_authenticated():
        profile = Profile.objects.get(user=request.user)
        notebooks = get_notebook_display_data(Notebook.objects.filter(user=request.user), profile.default_notebook)
    else:
        notebooks = None
    return render_to_response('notebook-list.html', {'notebooks': notebooks}, RequestContext(request))

def notebook_crud(request, action):
    errors = list()
    if request.user.is_authenticated() and request.method=="POST":
        data = request.POST
        errors = list()
        message = None
        # notebook_data = None
        profile = Profile.objects.get(user=request.user)
        if action == "new" or action =="edit":
            notebook_max_length = getattr(settings,'NOTEBOOK_NAME_LENGTH')
            notebook_name = data.get('name').strip()            
            notebook_name = (notebook_name[:notebook_max_length]) if len(notebook_name) > notebook_max_length else notebook_name
            if not notebook_name or notebook_name == "":
                errors.append("We need a notebook name. Give us a name!")
        if len(errors)==0:
            if action == "new":
                if profile.notebook_count < profile.notebook_limit:
                    notebook = Notebook(name=notebook_name, user=request.user, uniquekey=generate_uniquekey(getattr(settings, 'NOTEBOOK_KEY_LENGTH')))
                    notebook.save()
                    # notebook_data = {'id': notebook.id, 'name': notebook.name, 'key': notebook.uniquekey}
                    profile.notebook_count = profile.notebook_count + 1
                    profile.save()
                    message = "You have a new notebook!"
                else:
                    errors.append("That's a bummer but you exceeded your notebook limit.")
            else:
                try:
                    notebook = Notebook.objects.get(uniquekey=data.get('key'), user=request.user)
                    if action == "edit":
                        if notebook.name != notebook_name:
                            notebook.name = notebook_name
                            notebook.save()
                        message = "Your notebook was saved, alright!"
                        #    notebook_data = {'id': notebook.id, 'name': notebook.name, 'key': notebook.uniquekey}
                        # else:                        
                        #     notebook_data = {'same': True}
                    elif action == "delete":
                        if Quote.objects.filter(notebook=notebook, deleted=False).exists():
                            errors.append("There are quotes in this notebook, remove those first and try again.")
                        else:
                            if profile.default_notebook == notebook:
                                errors.append("Sorry but we can't delete your default notebook.")
                            else:
                                try:
                                    #notebook_data = {'id': notebook.id, 'name': notebook.name, 'key': notebook.uniquekey, 'deleted': True}
                                    Quote.objects.filter(notebook=notebook).delete()
                                    notebook_name = notebook.name
                                    notebook.delete()
                                    profile.notebook_count = profile.notebook_count - 1
                                    profile.save()
                                    message = "'%s' was deleted!" % notebook_name
                                except:
                                    errors.append("There was an undetected error with your request.")                                
                    elif action == "default":
                        profile.default_notebook = notebook
                        message = "'%s' is your new default notebook." % notebook.name
                        profile.save()
                except Notebook.DoesNotExist:
                    errors.append("Hey. The selected notebook is not yours! or or it may no exist.")   
    valid = True if len(errors)==0 else False
    response_data = dict(valid=valid, errors='• '.join(errors), message=message)
    return HttpResponse(json.dumps(response_data), content_type="application/json")


def change_notebook(request):
    errors = list()
    message = ""
    if request.user.is_authenticated() and request.method=="POST":
        # We need the quote identifier and the new notebook identifier.
        quote_key = request.POST.get('quote_key')
        notebook_key = request.POST.get('notebook_key')
        try:
            quote = Quote.objects.get(uniquekey=quote_key, user=request.user)
            notebook = Notebook.objects.get(uniquekey=notebook_key, user=request.user)
            if quote.notebook != notebook:
                quote.notebook = notebook
                quote.save()
                message = "You have changed the quote's notebook. Now it belongs, you know, somewhere I think."
            else:
                errors.append("You have to choose a different notebook.")
        except Quote.DoesNotExist:
            errors.append("Sorry. The quote doesn't exists or it doesn't belong to you.")
        except Notebook.DoesNotExist:
            errors.append("Sorry. The notebook doesn't exists or it doesn't belong to you.")
    valid = True if len(errors)==0 else False
    response_data = dict(valid=valid, errors='• '.join(errors), message=message)
    return HttpResponse(json.dumps(response_data), content_type="application/json")