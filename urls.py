from django.conf.urls import patterns, include, url
from django.views.generic import RedirectView, TemplateView
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
from quoffee.views import SupportPageView, ContactPageView
from quoffee.users.views import user_home
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'quoffee.quotes.views.index', name='index'),
    url(r'^favicon\.ico$', RedirectView.as_view(url='/static/images/app/app-logo-iDevice-greenbg.png')),
    url(r'^apple-touch-icon-precomposed\.png$', RedirectView.as_view(url='/static/images/app/app-logo-iDevice-greenbg.png')),
    url(r'^apple-touch-icon\.png$', RedirectView.as_view(url='/static/images/app/app-logo-iDevice-greenbg.png')),
    # url(r'^quoffee/', include('quoffee.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
    url(r'^login/$', 'quoffee.users.views.login'),
    url(r'^logout/$', 'quoffee.users.views.logout'),
    url(r'^bookmarklet/$', 'quoffee.quotes.views.bookmarklet_page'),
    url(r'^support/$', SupportPageView.as_view()),
    url(r'^contact/$', ContactPageView.as_view()),
    url(r'^complete-registration/$', 'quoffee.users.views.require_email', name='require_email'),

    url('', include('quoffee.context.urls', namespace='context')),
    url('', include('quoffee.quotes.urls', namespace='quotes')),
    url('', include('quoffee.users.urls', namespace='users')),
    url('', include('quoffee.notifications.urls', namespace='users')),
    url('', include('social.apps.django_app.urls', namespace='social')),
    url(r'^(?P<username>\w+)/$', user_home, name='user_home'),
)
