from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Author(models.Model):
	name = models.CharField(max_length=150)
	user = models.ForeignKey(User, blank=True, null=True) #user who first added the author.
	added_date = models.DateTimeField(auto_now_add=True)
	quote_count = models.IntegerField(default=0)

class Category(models.Model):
	name = models.CharField(max_length=100)
	name_es = models.CharField(max_length=100)
	added_date = models.DateTimeField(auto_now_add=True)