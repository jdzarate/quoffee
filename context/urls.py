from django.conf.urls import patterns, url

urlpatterns = patterns('quoffee.context.views',
	url(r'^api/search/authors/(?P<query>[^/]+)/$', 'search_authors', name='search_authors'),
)