import json
import pprint
from django.core import serializers
from django.core.serializers.json import Serializer
from django.http import HttpResponse
from django.shortcuts import render
from quoffee.context.models import Author
from quoffee.utils.serializers import AuthorSerializer

# Create your views here.

def search_authors(request, query):
	data = None
	if request.method == "GET" and request.user.is_authenticated():
		if query:
			authors = Author.objects.filter(name__icontains=query)
			data = AuthorSerializer(authors, many=True).data
	valid = True if data else False
	response_data = dict(valid=valid, data=data)
	return HttpResponse(json.dumps(response_data), content_type="application/json")

def save_author(name, user):
	striped_name = name.strip()
	try:
		author = Author.objects.get(name=striped_name).exists()
	except Author.DoesNotExists:
		author = Author(name=striped_name, user=user)
		author.save()
	return author.id