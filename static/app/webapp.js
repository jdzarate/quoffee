var defaultNotebook, notebookCount, notebookLimit, notebookAction="create";
var forceDefaultNotebook = false;

Messenger.options = {
    extraClasses: 'messenger-fixed messenger-on-top',
    theme: 'air'
}


$("#mymenu").mmenu({
	position: "left",
	zposition: "front",
	classes: "mm-quotemenu"
});
$("#quote-panel").mmenu({
	position: "right",
	zposition: "front",
	classes: ""
});



/*==============================================
			Notebook Actions
===============================================*/

function getNotebookProfileData(){
	ajaxCall('GET', url_users_search_profile , null, 'json', function(json){
		var response = eval(json);
		if (!response.error){
			defaultNotebook = response.data[0]['default_notebook'];
			notebookCount = response.data[0]['notebook_count'];
			notebookLimit = response.data[0]['notebook_limit'];
		};
	});
}

$('span[name="notebook-new"]').magnificPopup({
	type:'inline',
	midClick: true, // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
	callbacks:{
		open: function(){
			notebookAction = "create";
			$("#notebookform-key","#form-notebookform").val("");
			$("#notebookform-name","#form-notebookform").val("");
		}
	}
});			

function addNewNotebook(id, name){
	$("#quote-notebook").append("<option value='"+id+"'>"+name+"</option>");
	$("#notebook-list").append("<li>"+name+"</li>");
	$("#quote-notebook").easyDropDown("destroy");
	$("#quote-notebook").easyDropDown();
}

$("#btn-savenotebook").click(function(e){
	e.preventDefault();
	var action = ($("#notebookform-key").val()=="" || notebookAction=="create")?"create":"edit";
	var url = action=="create"?url_quotes_notebook_new:url_quotes_notebook_edit;
	var data = $("#form-notebookform").serialize();
	ajaxCall("POST", url, data, 'json', function(json){
		$.magnificPopup.close();
		var data = eval(json);
		if (data.valid){
			reloadNotebooks();
			Messenger().success({
				message: data.message,
				hideAfter: 5
			});
		}
		else Messenger().error({ message: data.errors, hideAfter: 5 });
	});
});

setNotebookItemActions();

function notebookMenuActions(){
	$('.mm-quotemenu-options').each(function() {
		$(this).qtip({
			show: 'click',
			/*hide: {
                fixed: true,
                delay: 100
            },*/
            hide: 'unfocus',
			content: {
				text: $(this).next('.quotemenu-options-content')
			},
			position:{
				my: 'bottom center',
				at: 'top center'
			},
			style: 'qtip-tipsy'
		});
	});
	setNotebookItemActions();				
}


function setNotebookItemActions(){
	$("span[name='nb-option-edit']").click(function(){
		$("#notebookform-key","#form-notebookform").val($(this).data('key'));
		$("#notebookform-name","#form-notebookform").val($(this).data('name'));
		$('.mm-quotemenu-options').qtip('hide');
		notebookAction="edit";
		$.magnificPopup.open({
		  items: {
		    src: '#notebook-form', // can be a HTML string, jQuery object, or CSS selector
		    type: 'inline',
		  }
		});
	});

	$("span[name='nb-option-delete']").click(function(){
		$("#notebookform-key","#form-notebookform").val($(this).data('key'));
		$("#notebookform-name","#form-notebookform").val("");
		$('.mm-quotemenu-options').qtip('hide');
		var data = $("#form-notebookform").serialize();
		ajaxCall("POST", url_quotes_notebook_delete, data, 'json', function(json){
			var data = eval(json);
			if (data.valid){
				reloadNotebooks();
				Messenger().success({
					message: data.message,
					hideAfter: 5
				});
			}
			else Messenger().error({ message: data.errors, hideAfter: 5 });
		});
	});

	$("span[name='nb-option-default']").click(function(){
		$("#notebookform-key","#form-notebookform").val($(this).data('key'));
		$("#notebookform-name","#form-notebookform").val("");
		$('.mm-quotemenu-options').qtip('hide');
		var data = $("#form-notebookform").serialize();
		ajaxCall("POST", url_quotes_notebook_default, data, 'json', function(json){
			var data = eval(json);
			if (data.valid){
				reloadNotebooks();
				Messenger().success({
					message: data.message,
					hideAfter: 5
				});
			}
			else Messenger().error({ message: data.errors, hideAfter: 5 });
		});
	});
}

function notebookArray(){
	var notebooks = new Array(), i=0;
	$("li", "#notebook-list").each(function(){
		notebooks[i++]= {
			'key': $(this).data('key'),
			'name': $(this).data('name'),
			'default': ($(this).data('default')=="true"?true:false)
		}
	});
	return notebooks.slice(0);
}


function reloadNotebooks(){
	ajaxCall("GET", url_quotes_notebook_list, null, 'html', function(data){
		if(data){
			$("#notebook-list").html(data);
			notebookMenuActions();
			var options = getNotebookHtmlOptions(notebookArray());
			$("#quote-notebook").html(options);
			$("#quote-notebook").easyDropDown("destroy");
			$("#quote-notebook").easyDropDown();
			getNotebookProfileData();
		}					
	});
}

function getNotebookHtmlOptions(notebookArray, filter, firstOption){
	var i, options="" ;
	if(firstOption) options+="<option></option>";
	for (i=0; i<notebookArray.length; i++){
		if(notebookArray[i].key==filter) continue;		
		options+="<option value='"+notebookArray[i].key+"'>"+notebookArray[i].name+"</option>";
	}
	return options;
}

/* change notebooks */

function clearChangeNotebookForm(){
	$("#changeNotebook-notebook").html(getNotebookHtmlOptions(notebookArray(), $("#quote-panel-container").attr("data-notebook"), true ));	
	$("#changeNotebook-notebook").easyDropDown({
		onChange: function(){
			ajaxCall("POST", '/api/quotes/change-notebook/', $("#form-choosenotebook").serialize(), 'json', function(json){
				if(json){
					var data = eval(json);
					$.magnificPopup.close();
					if (data.valid){						
						// show success message.
						var selectedNotebook=$("#changeNotebook-notebook").val();
						$("#quote-panel-container").attr("data-notebook", selectedNotebook);
						$("#changeNotebook-notebook").easyDropDown("destroy");
						Messenger().success({ message: data.message, hideAfter: 5 });
						if(forceDefaultNotebook){
							if(forceDefaultNotebook==selectedNotebook)
								$(".quote-container[data-id='"+$("#changeNotebook-quote").val()+"']", ".quote-feed").show();
							else
								$(".quote-container[data-id='"+$("#changeNotebook-quote").val()+"']", ".quote-feed").hide();
						}
					}
					else{
						Messenger().error({ message: data.errors, hideAfter: 5 });
					}
				}
			});
		}
	});
}





/*==============================
		Quotes Actions
================================*/

$('a[name="add-new-quote"]').magnificPopup({
	type:'inline',
	midClick: true, // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
	fixedContentPos: true,
	callbacks:{
		open: function(){
			if($("#new-quote").data('activated'))
				clearQuoteForm();
		}
	}
});

function getQuoteFromContainer(quotekey){
	ajaxCallLoadingBar("GET", url_quotes_get_data, {'key': quotekey}, 'html', function(data){
		if(data){
			$("#quote-panel").html(data);
			$("#quote-panel").trigger("open");
			twttr.widgets.load();						
				FB.XFBML.parse($('#quote-panel')[0]);
				$.getScript("http://static.bufferapp.com/js/button.js");

			setQuoteActions();
		}					
	});
}

function setQuoteActions(){
	$('a[name="change-notebook"]').magnificPopup({
		type:'inline',
		midClick: true, // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
		fixedContentPos: true,
		callbacks:{
		open: function(){
				$("#changeNotebook-quote").val($("#quote-panel-container").data('id'));
				clearChangeNotebookForm();
			}
		}		
	});
}

function deleteQuote(key){
	ajaxCall("POST", url_quotes_delete, {'key':key, 'csrfmiddlewaretoken': $.cookie('csrftoken')}, 'json', function(json){
		if(json){
			var data = eval(json);
			if (data.valid){
				$(".quote-container[data-id='"+data.key+"']").hide();
				$("#quote-panel").trigger("close");
				Messenger().post({
			  		message: data.message,
			  		type: 'info',
			  		hideAfter: 10,
	  				actions: {
				    	cancel: {
				      		label: 'Undo!',
				      		action: function() {
				      			Messenger().hideAll();
				        		ajaxCall("POST", url_quotes_delete, {'undo': true,'key':data.key, 'csrfmiddlewaretoken': $.cookie('csrftoken')}, 'json', function(json){
				        				if(json){							        					
				        					var data = eval(json);
				        					if(data.valid){
				        						Messenger().success({ message: data.message, hideAfter: 5 });
				        						$(".quote-container[data-id='"+data.key+"']").show();
				        					}
				        					else Messenger().error({ message: data.errors, hideAfter: 5 });
				        				}
				        		});
				      		}
					    }
			  		}
				});
			}
			else
				Messenger().error({ message: data.errors, hideAfter: 5 });
		}					
	});
}


/*===================
	Requotes 
===================*/
function requote(key){
	ajaxCall("POST", url_quotes_requote, {'key':key, 'csrfmiddlewaretoken': $.cookie('csrftoken')}, 'json', function(json){
		if(json){
			var data = eval(json);
			if (data.valid){
				Messenger().success({
					message: data.message,
					hideAfter: 5
				});
			}
			else{
				if(data.activation_error){
					$("#quote-panel").trigger("close");
					$.magnificPopup.open({
					  items: {
					    src: '#new-quote', // can be a HTML string, jQuery object, or CSS selector
					    type: 'inline',
					  }
					});
				}
				else
					Messenger().error({ message: data.errors, hideAfter: 5 });
			}
		}					
	});
}


function get_quotes_index(page){
	ajaxCall("GET", 'api/quotes/get/index', {'page':page, 'csrfmiddlewaretoken': $.cookie('csrftoken')}, 'html', function(html){
		if(html){
			$("#quotes-container").append(html);
		}
	});
}

function loadMoreQuotes(path, page){
	$(".more-quotes-icon").toggleClass('fa-spin');
	ajaxCall("GET", path, {'page':page, 'csrfmiddlewaretoken': $.cookie('csrftoken')}, 'html', function(html){
		if(html){
			$(".more-quotes").hide();
			$(".more-quotes-icon").toggleClass('fa-spin');
			$("#quotes-container").append(html);
		}
	});
}

function readAllNotifications(){
	$(".side-notification-a, .main-notification").remove();
	location.href='/notifications';
}



$('.mm-quotemenu-options').each(function() {
	$(this).qtip({
		show: 'click',
		/*hide: {
            fixed: true,
            delay: 100
        },*/
        hide: 'unfocus',
		content: {
			text: $(this).next('.quotemenu-options-content')
		},
		position:{
			my: 'bottom center',
			at: 'top center'
		},
		style: 'qtip-tipsy'
	});
});