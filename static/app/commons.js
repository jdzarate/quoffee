function ajaxCall(method,url,data,dataType,callback){
	$.ajax({
		url: url,
		type: method,
		data: data,
		dataType: dataType,
		beforeSend: function(){
			$('button').attr('disabled',true);
  			/*$('#loader').show();*/
		},
		success: function(res){
			$('button').attr('disabled',false);
  			/*$('#loader').hide();*/
			callback(res);
		}
	});
}

function ajaxCallLoadingBar(method,url,data,dataType,callback){
	$.ajax({
		url: url,
		type: method,
		data: data,
		dataType: dataType,
		beforeSend: function() {
			$('button').attr('disabled',true);
            if ($("#loadingbar").length === 0) {
              	$('body').append("<div id='loadingbar'></div>")
              	$("#loadingbar").addClass("waiting").append($("<dt/><dd/>"));
              	$("#loadingbar").width((50 + Math.random() * 30) + "%");
            }
          },
		success: function(res){
			$('button').attr('disabled',false);
  			/*$('#loader').hide();*/
			callback(res);
		}
	}).always(function() {
		$("#loadingbar").width("101%").delay(200).fadeOut(400, function() {
               $(this).remove();
           });       
        });
}