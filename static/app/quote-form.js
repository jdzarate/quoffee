function targetopener(mylink, closeme, closeonly)
{
	if (! (window.focus && window.opener))return true;
	window.opener.focus();
	if (! closeonly)window.opener.location.href=mylink.href;
	if (closeme)window.close();
	return false;
}

$("#quote-text").autosize();
$("#quote-category").selectize({
	create: true
});
$("#quote-author").selectize({
	create: true,
	valueField: 'id',
	labelField: 'name',
	searchField: 'name',
	render:{
		option: function(item, escape){
			return '<div>'+escape(item.name)+'</div>';
		}
	},
	load: function(query, callback){
		if (!query.length) return callback();
		$.ajax({
			url: '/api/search/authors/'+encodeURIComponent(query),
			type: 'GET',
			error: function(){
				callback();
			},
			success: function(res){
				callback(res.data.slice(0,10));
			}
		});
	}
});
$("#quote-tags").selectize({
	delimeter:',',
	persist: false,
	maxItems: 3,
	create: function(input){
		return{
			value: input,
			text: input
		}
	}
});


$("#btn-newnotebook").click(function(e){
	e.preventDefault();
	if(notebookCount<notebookLimit){
		$("#quote-newnotebook, #btn-cancelnotebook").show();
		$("#btn-newnotebook").hide();	
	}
	else{
		Messenger().error({ message: "That's a bummer but you exceeded your notebook limit.", hideAfter: 5 });
	}
});

$("#btn-cancelnotebook").click(function(e){
	e.preventDefault();
	$("#quote-newnotebook, #btn-cancelnotebook").hide();
	$("#quote-newnotebook").val("");
	$("#btn-newnotebook").show();				
});


$("#btn-insertquote").click(function(e){
	e.preventDefault();
	if(validateQuoteForm()){
		var url = $(this).data('url');
		var data = $('#form-newquote').serialize();
		var callback = function(json){
			$(".loading-new-quote").hide();
			var data = eval(json);
			var html = '';
				var errors = data.errors;
			if(data.valid){
				if ($("#btn-insertquote").data('bookmarklet')==true){
					$("#new-quote").html("<p>Yei! Quote Saved!<br><a onclick='return targetopener(this,true);' href='http://quoffee.com/q/"+data.quotekey+"/'>Click here to check it out!</a></p>");
				}
				else{
					Messenger().success({
						message: data.message, hideAfter: 5
					});
					if(forceDefaultNotebook && forceDefaultNotebook==$("#quote-notebook").val()){
						ajaxCall('GET', "/quote/get/", {'uniquekey': data.quotekey}, 'html', function(html){
							$("#notebook-quote-content").prepend(html);
						});
					}	
					$.magnificPopup.close();
					clearQuoteForm();
					if(data.is_new_notebook)
						reloadNotebooks();
				}
				
			}
			else{
				for (key in errors){
	  				html+="<li>"+errors[key]+"</li>";
	  				$('#quote-errors-list').html(html);
	  				$('#quote-errors').show();
	  			}
			}
		};
		$(".loading-new-quote").show();
		ajaxCall('POST', url, data,'json',callback);
	}
});

function validateQuoteForm(){
	var errors = "";
	if($.trim($("#quote-text").val())=="")
		errors+="<li>"+"Come on! At least type the quote text."+"</li>";
	if(errors.length>0){
		$('#quote-errors-list').html(errors);
	  	$('#quote-errors').show();
	  	$('#quote-text').focus();
	  	return false;
	}
	return true;
}

function clearQuoteForm(){
	$('#quote-errors').hide();
	$("#quote-category")[0].selectize.clear();
	$("#quote-author")[0].selectize.clear();
	$("#quote-tags")[0].selectize.clear();
	$("#quote-text").val("");
	$("#btn-cancelnotebook").click();
	if(forceDefaultNotebook){
		$("#quote-notebook").val(forceDefaultNotebook.toString());
		$("#quote-notebook").easyDropDown("select",forceDefaultNotebook.toString());
	}
	else{
		$("#quote-notebook").val(defaultNotebook.toString());
		$("#quote-notebook").easyDropDown("select",defaultNotebook.toString());
	}
}