import random, string, json
from django.template.defaultfilters import slugify
from django.db.models import Count
from quoffee.users.models import Profile
from quoffee.quotes.models import ReQuote, Notebook
from quoffee.tags.models import TagMap, Tag
from quoffee.notifications.models import Notification

def get_quote_display_data(quote):
    try:
        profile = Profile.objects.get(user=quote.user)
    except Profile.DoesNotExist:
        profile = None
    try:
    	quote_meta = json.loads(quote.meta)
    except:
    	quote_meta = None
    quote = dict(uniquekey=quote.uniquekey,text=quote.text, author=quote.author, category=quote.category, 
    	custom_category=quote.custom_category, requoted=quote.requoted, requote_count=quote.requote_count, 
    	added_date=quote.added_date, notebook=quote.notebook, deleted=quote.deleted, meta=quote_meta, username=quote.user.username)
    quote['profile'] = dict(display=profile.display) if profile else None
    return quote

def get_notebook_display_data(notebooks, default_notebook):
	notebook_list = list()
	for item in notebooks:
		is_default = True if item.id == default_notebook.id else False
		notebook_list.append({'uniquekey': item.uniquekey, 'name': item.name, 'default': is_default })
	return notebook_list

def get_quote_share_str(quote_text, author, quote_key):
	txt = quote_text + (' - '+author.name if author else '')
	# tweet length: 140, url length: 20
	txt_buffer = txt if len(txt)<117 else txt[:114]+'...'
	txt = txt[:114]+'... '+'http://quoffee.com/q/'+quote_key if len(txt)>140 else txt
	return txt, txt_buffer

def get_quote_page_title(quote_text, quote_author):
	return 'Quoffee / '+(quote_author.name+'/' if quote_author else '') + quote_text[:40]


def get_quote_page_data(user,quote):
	data = None
	if quote.deleted:
		data = {'deleted': True}
	else:
		quote_display_data = get_quote_display_data(quote)
		is_quote_user = True if quote.user == user else False            
		requotes =  ReQuote.objects.filter(quote=quote, deleted=False)
		requotes_data = list()
		quote_user_profile = Profile.objects.get(user=quote.user)
		public = quote_user_profile.public
		for item in requotes:
			profile = Profile.objects.get(user=item.user)
			requotes_data.append({'display': profile.display, 'username': item.user.username })
		share_str, share_str_buffer = get_quote_share_str(quote.text, quote.author, quote.uniquekey)
		data = {'is_quote_user': is_quote_user, 'quote': quote_display_data, 'requotes': requotes_data, 
				'public': public, 'share_str': share_str, 'share_str_buffer': share_str_buffer,
				'page_title': get_quote_page_title(quote.text, quote.author)
				}
		tagmap = TagMap.objects.filter(quote=quote)
		tags_data = list()
		for item in tagmap:
			tags_data.append({'name': item.tag.name, 'slug': item.tag.slug})
		if len(tags_data)>0:
			data['tags'] = tags_data
	return data

def get_is_user_context_data(user):
	try:
		profile = Profile.objects.get(user=user)
		notebooks = get_notebook_display_data(Notebook.objects.filter(user=user), profile.default_notebook)
		data_user = dict(id=user.id, name=user.first_name+' '+user.last_name, display=profile.display, username=user.username)        
		tagmap = TagMap.objects.values('tag').annotate(tag_quote_count=Count('tag')).filter(quote__user=user).order_by('-tag_quote_count')[:5]
		tags = Tag.objects.filter(id__in=[item['tag'] for item in tagmap])
		notifications = Notification.objects.filter(user=user, status='N').count()
		context_data = {'user': data_user, 'is_user': True, 'notebooks': notebooks, 'profile': profile,'tags': tags, 'notifications_count': notifications}
	except Exception,e:
		context_data = None
	return context_data

def generate_uniquekey(length):
	return ''.join(random.choice(string.ascii_letters + string.digits) for x in range(length))


def slugify_unique(value, model, slugfield="slug"):
	suffix = 0
	potential = base = slugify(value)
	while True:
		if suffix:
			potential = "-".join([base, str(suffix)])
		if not model.objects.filter(**{slugfield: potential}).count():
			return potential
            # we hit a conflicting slug, so bump the suffix & try again
		suffix += 1