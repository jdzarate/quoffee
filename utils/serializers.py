from rest_framework import serializers
from django.contrib.auth.models import User
from quoffee.context.models import Author
from quoffee.users.models import Profile
from quoffee.quotes.models import ReQuote
'''----------------
	serializers
----------------'''
class AuthorSerializer(serializers.ModelSerializer):
	class Meta:
		model = Author
		fields = ('id','name')


class ProfileSerializer(serializers.ModelSerializer):
	default_notebook = serializers.Field(source='default_notebook.uniquekey')
	class Meta:
		model = Profile
		fields = ('id','display','default_notebook', 'notebook_count', 'notebook_limit')

class PublicProfileSerializer(serializers.ModelSerializer):
	class Meta:
		model = Profile
		fields = ('display',)

#display img, profile url, name?=not now.
class UserProfileSerializer(serializers.ModelSerializer):
	profile = PublicProfileSerializer(many=True)
	class Meta:
		model = User
		fields = ('first_name','last_name', 'profile')

class ReQuoteSerializer(serializers.ModelSerializer):
	user = UserProfileSerializer(many=True)
	# the only thing missing is quote user and requote users.
	class Meta:
		model = ReQuote
		fields = ('user',)