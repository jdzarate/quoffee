from django.db import models
from django.contrib.auth.models import User
from quoffee.quotes.models import Quote

# Create your models here.
class Tag(models.Model):
	name = models.CharField(max_length=100)
	user = models.ForeignKey(User)
	added_date = models.DateTimeField(auto_now_add=True)
	slug = models.CharField(max_length=200)

class TagMap(models.Model):
	tag = models.ForeignKey(Tag)
	quote = models.ForeignKey(Quote)