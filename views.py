from django.views.generic.base import TemplateView
from quoffee.utils.methods import get_is_user_context_data

class ContactPageView(TemplateView):
    template_name = "contact.html"

    def get_context_data(self, **kwargs):
        is_user = self.request.user.is_authenticated()
        context = super(ContactPageView, self).get_context_data(**kwargs)
        if is_user:
            data = get_is_user_context_data(self.request.user)
            for item in data:
                context[item] = data[item]
        return context

class SupportPageView(TemplateView):
    template_name = "support.html"

    def get_context_data(self, **kwargs):
        is_user = self.request.user.is_authenticated()
        context = super(SupportPageView, self).get_context_data(**kwargs)
        if is_user:
            data = get_is_user_context_data(self.request.user)
            for item in data:
                context[item] = data[item]
        return context