from django.db import models
from django.contrib.auth.models import User
from django.dispatch import receiver
from django.utils.encoding import iri_to_uri
from django.db.models.signals import post_save
from templated_email import send_templated_mail
from quoffee.quotes.models import Notebook
from rest_framework import serializers

# Create your models here.
class Profile(models.Model):
	user = models.ForeignKey(User)
	display = models.CharField(null=True, max_length=150)
	default_notebook = models.ForeignKey(Notebook)
	notebook_count = models.IntegerField(default=1)
	notebook_limit = models.IntegerField(default=5)
	public = models.BooleanField(default=True)
	activated = models.BooleanField(default=False)


class RegistrationProfile(models.Model):
	user = models.ForeignKey(User)
	activation_key = models.CharField(max_length=30)
	activation_email = models.CharField(max_length=150)
	activated = models.BooleanField(default=False)


'''-----------
	Signals
------------'''
@receiver(post_save, sender=RegistrationProfile)
def registration_profile_save(sender, instance, created,**kwargs):
	if created:
		#send user an email for validation.
		send_templated_mail(
			template_name='activation',
			from_email='info@quoffee.com',
			recipient_list=[instance.activation_email],
			context={
				'user': instance.user,
				'key': instance.activation_key,
				'email': iri_to_uri(instance.activation_email)
			}
		)