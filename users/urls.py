from django.conf.urls import patterns, url

urlpatterns = patterns('quoffee.users.views',
	url(r'^api/search/profiles/', 'search_profile', name='search_profile'),
	url(r'^user/activation/', 'user_activation', name='user_activation'),
	url(r'^(?P<username>\w+)/nb/(?P<notebook>\w+)/$', 'user_notebook', name='user_notebook'),
	url(r'^(?P<username>\w+)/tags/(?P<slug>[-\w]+)/$', 'user_tags', name='user_tags'),
)