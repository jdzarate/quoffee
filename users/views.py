import json
from django.conf import settings
from django.contrib.auth import logout as auth_logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpResponse, Http404
from django.shortcuts import render, render_to_response, redirect
from django.template import RequestContext
from social.backends.google import GooglePlusAuth
from quoffee.users.models import Profile, RegistrationProfile
from quoffee.quotes.models import Notebook, Quote
from quoffee.tags.models import Tag, TagMap
from quoffee.utils.serializers import ProfileSerializer
from quoffee.utils.methods import get_quote_display_data, get_is_user_context_data
# Create your views here.

def login(request):
    """Home view, displays login mechanism"""
    if request.user.is_authenticated():
        return redirect('index')
    return render_to_response('login.html', {
        'plus_id': getattr(settings, 'SOCIAL_AUTH_GOOGLE_PLUS_KEY', None)
    }, RequestContext(request))

def logout(request):
    auth_logout(request)
    return render_to_response('login.html', {}, RequestContext(request))

def require_email(request):
    if request.method == "POST":
        request.session['saved_email'] = request.POST.get('email')
        backend = request.session['partial_pipeline']['backend']
        return redirect('social:complete', backend=backend)
    return render_to_response('complete_registration.html', RequestContext(request))

def search_profile(request):
    data = None
    if request.method == "GET" and request.user.is_authenticated():
        profile = Profile.objects.filter(user=request.user)
        data = ProfileSerializer(profile, many=True).data
    valid = True if data else False
    response_data = dict(valid=valid, data=data)
    return HttpResponse(json.dumps(response_data), content_type="application/json")

@login_required
def home(request):
    """Login complete view, displays user data"""
    scope = ' '.join(GooglePlusAuth.DEFAULT_SCOPE)
    return render_to_response('home.html', {
        'user': request.user,
        'plus_id': getattr(settings, 'SOCIAL_AUTH_GOOGLE_PLUS_KEY', None),
        'plus_scope': scope
    }, RequestContext(request))


def user_notebook(request, username, notebook):
    is_user = True if request.user.is_authenticated() else False
    if is_user:
        context_data = get_is_user_context_data(request.user)
    else:
        context_data = {'is_user': False}
    try:
        userbyun = User.objects.get(username=username)
        try:
            notebook = Notebook.objects.get(user=userbyun, uniquekey=notebook)
            profilebyun = Profile.objects.get(user=userbyun)
            if (profilebyun.public or request.user==userbyun) and request.method=='GET':
                is_notebook_owner = True if request.user==userbyun else False
                notebooks = Notebook.objects.filter(user=userbyun)
                quote_list = None
                if request.GET.get('tag'):
                    tag_name = request.GET.get('tag').lower()
                    try:
                        tag = Tag.objects.get(name=tag_name)
                        quote_list = TagMap.objects.values_list('quote_id', flat=True).filter(quote__user=userbyun, tag=tag)
                    except Tag.DoesNotExist:
                        pass
                if quote_list:
                    quotes = Quote.objects.filter(notebook=notebook, deleted=False, id__in=quote_list).order_by('-added_date')
                else:
                    quotes = Quote.objects.filter(notebook=notebook, deleted=False).order_by('-added_date')
                quote_array = list()
                for item in quotes:
                    quote_array.append(get_quote_display_data(item))
                context_data['is_notebook_owner'] = is_notebook_owner
                context_data['notebook_user'] = userbyun
                context_data['notebook_user_profile'] = profilebyun
                context_data['notebook_user_notebooks'] = notebooks
                context_data['quotes'] = quote_array
                context_data['notebook'] = notebook
                return render_to_response('notebook.html', context_data, RequestContext(request))
            else:
                raise Http404
        except Notebook.DoesNotExist:
            raise Http404
    except User.DoesNotExist:
        raise Http404


def user_tags(request, username, slug):
    is_user = True if request.user.is_authenticated() else False
    if is_user:
        context_data = get_is_user_context_data(request.user)
    else:
        context_data = {'is_user': False}
    try:
        userbyun = User.objects.get(username=username)
        quote_array = list()
        try:
            tag = Tag.objects.get(slug=slug)
            profilebyun = Profile.objects.get(user=userbyun)
            if profilebyun.public or request.user==userbyun:
                is_notebook_owner = True if request.user==userbyun else False
                notebooks = Notebook.objects.filter(user=userbyun)
                quote_list = Quote.objects.values_list('id', flat=True).filter(user=userbyun, deleted=False).order_by('-added_date')
                quote_map = TagMap.objects.filter(tag=tag, quote__in=quote_list)
                for item in quote_map:
                    quote_array.append(get_quote_display_data(item.quote))
                context_data['is_notebook_owner'] = is_notebook_owner
                context_data['quotes'] = quote_array
                context_data['notebook_user_notebooks'] = notebooks
            context_data['notebook_user_profile'] = profilebyun
            context_data['tag'] = tag
        except Tag.DoesNotExist:
            pass #no results.
        context_data['notebook_user'] = userbyun
        return render_to_response('user_tags.html', context_data, RequestContext(request))
    except User.DoesNotExist:
        raise Http404

def user_home(request, username):
    try:
        profile = Profile.objects.get(user=User.objects.get(username=username))
        url = '/%s/nb/%s/'% (username, profile.default_notebook.uniquekey)
        return redirect(url)
    except Exception,e:
        raise Http404

def user_activation(request):
    if request.method=='GET':
        email = request.GET.get('email')
        key = request.GET.get('key')
        try:
            registration = RegistrationProfile.objects.get(activation_email=email, activation_key=key)
            if registration.activated == False:
                profile = Profile.objects.get(user=registration.user)
                registration.activated = profile.activated = True
                profile.save()
                registration.save()
            context = {'error': False}
        except RegistrationProfile.DoesNotExist:
            context = {'error': True}
        return render_to_response('activation.html', context, RequestContext(request))
    else:
        raise Http404