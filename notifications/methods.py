'''
	Methods for notifications
'''
from quoffee.notifications.models import Notification

def read_all_notifications(user):
	try:
		notifications = Notification.objects.filter(user=user, status='N')
		for item in notifications:
			item.status = 'R'
			item.save()
		return True
	except Exception,e:
		print e
		return False

def read_these_notifications(user, notification_ids):
	notifications = Notification.objects.filter(user=user, status='N', id__in=notification_ids)
	for item in notifications:
		item.status = 'R'
		item.save()

def unread_these_notifications(user, notification_ids):
	notifications = Notification.objects.filter(user=user, status='R', id__in=notification_ids)
	for item in notification:
		item.status = 'N'
		item.save()

def get_not_read_notifications(user):
	return Notification.objects.filter(user=user, status='N')