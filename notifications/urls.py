from django.conf.urls import patterns, url

urlpatterns = patterns('quoffee.notifications.views',
	url(r'^notifications/', 'get_notifications_page', name='get_notifications_page'),
	url(r'^api/notifications/read/', 'read_notifications', name='read_notifications'),
)