from django.db import models
from django.contrib.auth.models import User

''' 
Not implemented yet
-------------------
class NotificationType(models.model):
	name = models.CharField(max_length=20)
	template = models.CharField(max_length=200) #django template name
	# from django.template.loader import render_to_string
'''
class Notification(models.Model):
	user = models.ForeignKey(User)
	notif_type = models.PositiveSmallIntegerField() # 1-Requotes.
	notif_item = models.IntegerField() #id of the object (can be a quote or other stuff.)
	status = models.CharField(max_length=1,choices=(('R','Read'),('N','Not Read'), ('A','Archived')), default='N')
	sender = models.ForeignKey(User, blank=True, null=True,related_name='user_sender')
	added_date = models.DateTimeField(auto_now_add=True)