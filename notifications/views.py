import json
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render, render_to_response, redirect
from django.template import RequestContext
from quoffee.utils.methods import get_is_user_context_data
from quoffee.notifications.models import Notification
from quoffee.notifications.methods import read_all_notifications
from quoffee.quotes.models import Quote
from quoffee.users.models import Profile


# Create your views here.
def get_notifications_page(request):
	if request.user.is_authenticated():
		context_data = get_is_user_context_data(request.user)
		notifications = Notification.objects.filter(user=request.user).order_by('-added_date')
		# asumming only requotes:
		notifications_list = list()
		for item in notifications:
			notif_item = dict()
			try:
				data = Quote.objects.get(pk=item.notif_item).text
				data = data[:100]+'...' if len(data)>100 else data
				sender_display = Profile.objects.get(user=item.sender).display
				notifications_list.append({
					'type':'requote', 'data': data, 'sender': item.sender, 'status': item.status,
					'display': sender_display, 'dateformat': item.added_date.strftime("%b %d, %Y")
					})
			except Exception, e:
				print e
				pass
		context_data['notifications'] = notifications_list
		return render_to_response('notifications/notifications_page.html', context_data, RequestContext(request))
	else:
		return redirect('/')

@login_required
def read_notifications(request):
	errors = list()
	if request.method=='POST' and request.user.is_authenticated():
		if not read_all_notifications(request.user):
			errors.append('We catch an unexpected error with your request.')
	response = {'valid': (len(errors)==0), 'errors':errors}
	json_response = json.dumps(response)
	return HttpResponse(json_response, content_type='application/javascript')